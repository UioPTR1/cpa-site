import React from "react";

const Logo = () => {
  return (
    <div className="logo">
      <h1>אילן בנימין - רו"ח</h1>
      <h2>Certified Public Accountant</h2>
    </div>
  );
};

export default Logo;
