import React, { useState } from "react";
import axios from 'axios';

const Contact = () => {
  const [serverState, setServerState] = useState({
    submitting: false,
    status: null
  });
  const handleServerResponse = (ok, msg, form) => {
    setServerState({
      submitting: false,
      status: { ok, msg }
    });
    if (ok) {
      form.reset();
    }
  };
  const handleOnSubmit = e => {
    e.preventDefault();
    const form = e.target;
    setServerState({ submitting: true });
    axios({
      method: "post",
      url: "https://formspree.io/xyyddbkd",
      data: new FormData(form)
    })
      .then(r => {
        handleServerResponse(true, "Thanks!", form);
      })
      .catch(r => {
        handleServerResponse(false, r.response.data.error, form);
      });
  };

  return (
    <div>
      <h1 className="contact-head">צור קשר</h1>
      <form
        className="contact"
        onSubmit={handleOnSubmit}
      >
        <input
          dir="rtl"
          className="inputHead"
          type="email"
          name="mailAddress"
          id="mail"
          placeholder="דואר אלקטרוני"
        />
        <input
          dir="rtl"
          className="inputHead"
          type="text"
          placeholder="מספר טלפון"
          name="phone"
          id="phone"
        />
        <input
          dir="rtl"
          className="inputHead"
          type="text"
          name="name"
          placeholder="שם מלא"
          id="fullname"
        />
        <textarea className="text" dir="rtl" placeholder="פרטים..." name="content"></textarea>
        <input type="submit" value="שלח" className="submitButton" disabled={serverState.submitting}/>
      </form>
    </div>
  );
};

export default Contact;
