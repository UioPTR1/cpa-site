import React from "react";
import Logo from "./Logo";
import FacebookIcon from "@material-ui/icons/Facebook";
import { Link } from "react-router-dom";

const Navigation = () => {
  const navStyle = {
    color: "rgb(35, 35, 35)",
    padding: "0",
    margin: "0",
    textDecoration: "none",
  };

  return (
    <ul className="navbar">
      <li>
        <Link style={navStyle} to="/">
          <Logo />
        </Link>
      </li>
      <Link style={navStyle} to="/offer">
        <li className="link">השירותים שלנו</li>
      </Link>
      <Link style={navStyle} to="/about">
        <li className="link">אודות</li>
      </Link>
      <Link style={navStyle} to="/articles">
        <li className="link">מאמרים</li>
      </Link>
      <Link style={navStyle} to="/contact">
        <li className="link">צור קשר</li>
      </Link>
      <li className="link">קישורים ומחשבונים</li>
        <li className="f fac">
          <a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer">
          <FacebookIcon />
          </a>
        </li>
    </ul>
  );
};

export default Navigation;
