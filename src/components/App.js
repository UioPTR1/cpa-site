import React, { Suspense, lazy } from "react";
import Navigation from "./Navigation";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./Home";
import Articles from "./Articles";
import Offer from "./Offer";

const Contact = lazy(() => import("./Contact"));
const About = lazy(() => import("./About"));

const App = () => {
  return (
    <Router>
      <Navigation />
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/offer" component={Offer} exact />
        <Route path="/articles" component={Articles} exact />
        <Suspense fallback={<div>טוען...</div>}>
          <Route path="/about" component={About} exact />
          <Route path="/contact" component={Contact} exact />
        </Suspense>
      </Switch>
      {/* <Footer/> */}
    </Router>
  );
};

export default App;
